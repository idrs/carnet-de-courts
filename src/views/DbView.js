import m from "mithril";
import Courts from "../models/Courts"
import {
  marked
}
from "marked";
import dompurify from "dompurify";

function md2html(txt) {
  return dompurify.sanitize(marked.parse(txt));
}

function comment(court) {
    var active = false;
    return {
      
      view: function() {
        
        return m(".comment.italic",
          m('span.clique', {
            onclick: () => {
              console.log("clicked: active="+active);
             if(active)
             {active = false;}
             else { active = true;}
             console.log("clicked: active="+active);
            }
          }, '+'),
          m(active ? 'span.active' : 'span.hidden', m.trust(md2html(court.comment)))
        )
      }
    }
  }


  function publication(court){
  
  var optinput = {
                    name: "public",
                    type:"checkbox",
                    onchange:()=>{
                      if(court.public){
                        console.log(court);
                        Courts.unmakePublic(court._id);
                      }
                      else{
                        Courts.makePublic(court._id);  
                      }
                    }
                  }
  if(court.public){optinput["checked"]=null;}
  return   m("span.publication",
                m(".checkbox",
                  [m("input",optinput),
                  m("label",{for:"public"},"publié")]
                )
            );
  }
  
  function contenu(court) {
    //console.log(court)
    var trid = court.type == "trident" 
    return [
      //m(".forme",sonnet.forme),
      m(".poeme.block",
        //court.title!=""?m(".titre",court.title):null,
        m(".strophe", court.content.split(/\r?\n/)
          .map((v, i) => m('p.vers' + ((trid && (i != 1)) ? '.plus' : ''), ((trid && (i == 1)) ?
            '⊗ ' : '') + v)))),
      m(comment(court))
    ]
  }

function DbView() {
  Courts.getDb();
  return {
    view: function() {
      //   console.log("liste: ",Object.values(Courts.liste));
      return m("ul",
        m("li",m("b","db")),
        Courts.dbList
        .map( 
          (s,i) => m('li',JSON.stringify(s))
        ),
        
        m("li",m("b","publicdb")),
        Courts.publicDbList
        .map( 
          (s,i) => m('li',i+". "+JSON.stringify(s))
        ),
        m("li",m("b","rdb")),
        Courts.rdbList
        .map( 
          (s,i) => m('li',JSON.stringify(s))
        ),
        
        m("li",m("b","rpublicdb")),
        Courts.rpublicDbList
        .map( 
          (s,i) => m('li',i+". "+JSON.stringify(s))
        )
        
        )
            
    }
  }
};
module.exports = DbView;

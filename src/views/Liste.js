import m from "mithril";
import Courts from "../models/Courts"
import {
  marked
}
from "marked";
import dompurify from "dompurify";

function md2html(txt) {
  return dompurify.sanitize(marked.parse(txt));
}

const debug = false;

function comment(court) {
    var active = false;
    return {
      
      view: function() {
        
        return m(".comment.italic",
          m('span.clique', {
            onclick: () => {
              console.log("clicked: active="+active);
             if(active)
             {active = false;}
             else { active = true;}
             console.log("clicked: active="+active);
            }
          }, '+'),
          m(active ? 'span.active' : 'span.hidden', m.trust(md2html(court.comment)))
        )
      }
    }
  }


  function publication(court){
  
  return   m("span.publication.tag",{
    onclick:()=>{Courts.togglePublic(court._id)}
  },
                court.public?"partagé":"privé"
            );
  }
  
  function contenu(court) {
    //console.log(court)
    var trid = court.type == "trident" 
    return [
      //m(".forme",sonnet.forme),
      m(".poeme.block",
        //court.title!=""?m(".titre",court.title):null,
        m(".strophe", court.content.split(/\r?\n/)
          .map((v, i) => m('p.vers' + ((trid && (i != 1)) ? '.plus' : ''), ((trid && (i == 1)) ?
            '⊗ ' : '') + v)))),
      m(comment(court))
    ]
  }

function Liste() {
  function versDate(n) {
    var options = {
      year: 'numeric',
      month: 'long',
      day: 'numeric'
    };
    const d = new Date(n);
    return d.toLocaleDateString("fr-FR", options);
  }

  function makeAndGo(){
                var newid = Courts.newCourt();
                m.route.set("court/"+newid);
  }
  
      var visible = 20;
  return {
    view: function() {
      //   console.log("liste: ",Object.values(Courts.liste));
      //console.log(Courts)
      if(Courts.isLoading){
      setTimeout(m.redraw,300);}
      
      return m(".container.is-max-desktop",
                m(".box",{onscroll:()=>{console.log("scrolling !");}},
                  m(".action",{onclick:makeAndGo}, "+ Nouveau court")),
                m("ul",
        Object.values(Courts.liste)
        .map((s,i)=>[s,i]).reverse().slice(0,visible).map((arr) =>
            m('li.box',
              m(m.route.Link,
                {
                  selector:'div',
                  class:"bold",
                  href:"/court/" + arr[0]._id}, 
                  (arr[1] + 1) + ". " + arr[0].title
              ),
              publication(arr[0]),
              contenu(arr[0]),
              m(".italic.small", versDate(parseInt(arr[0]._id, 16))),
             // m("span.bold", arr[0].type || "?"),
              //m("div",s.content)
            )),
            debug?Object.values(Courts.listepub)
        .map((s,i)=>[s,i]).reverse().slice(0,visible).map((arr) =>
            m('li.box',
              m(m.route.Link,
                {
                  selector:'div',
                  class:"bold",
                  href:"/court/" + arr[0]._id}, 
                  (arr[1] + 1) + ". " + arr[0].title
              ),
              publication(arr[0]),
              contenu(arr[0]),
              m(".italic.small", versDate(parseInt(arr[0]._id, 16))),
             // m("span.bold", arr[0].type || "?"),
              //m("div",s.content)
            )):null,
            (Object.values(Courts.liste).length>visible)?
              m("li.box",{onclick:()=>{visible=visible+10}},m(".bold","Suivants")):
                null
              ))
    }
  }
};
module.exports = Liste;

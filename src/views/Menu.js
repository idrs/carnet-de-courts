import m from "mithril"
import Courts from "../models/Courts"

function Menu() {
  var menulinks = ["config"];
  var modal = Courts.isLoading?m(".loader","En attente..."):null;
  var isActive = false;
  const toggleActiveMenu = ()=> isActive = !isActive;
  return {
    view: function(vnode){
      //console.log(vnode)
      if(vnode.children[0].attrs.id && Object.values(Courts.liste).length > 0){
        var envue = vnode.children[0].attrs.id;
        var courtTitle = m(m.route.Link, {class:"navbar-item", href: "/court/"+envue},Courts.liste[envue]?Courts.liste[envue].title:'');
        }
      return m("nav#topbar.navbar",{role:"navigation","aria-label":"main navigation"},
        m(".navbar-brand",
          m(m.route.Link,{class:"navbar-item is-size-5 has-text-weight-bold",href: "/" },"Carnet de courts"),
          m('a.navbar-burger'+(isActive?'.is-active':''),{onclick:toggleActiveMenu,role:"button",'aria-label':'menu','aria-expanded':'false','data-target':"navbarReste"},
            [m('span',{'aria-hidden':'true'}),
              m('span',{'aria-hidden':'true'}),
              m('span',{'aria-hidden':'true'})])),
        m('#navbarReste.navbar-menu'+(isActive?'.is-active':''),
          m('.navbar-start',
            courtTitle),
          m('.navbar-end',
            menulinks.map(
              l=>m(m.route.Link,{class:"navbar-item",href: "/"+l },l)
            )
          )
        )
        //modal
      );
    }
  }
  
}

module.exports = Menu;

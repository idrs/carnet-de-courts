import m from "mithril"
import Menu from "./Menu"
import Courts from "../models/Courts"

function Layout() {
  return {
    view: function(vnode){
      //console.log(vnode.children)
      
      return [
        m(Menu,vnode.children),
        m("#content.is-medium",vnode.children)
        ];
    }
  }
}


module.exports = Layout


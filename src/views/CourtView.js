import m from "mithril";
import Courts from "../models/Courts"
import range from 'underscore/modules/range';


import throttle from 'underscore/modules/throttle';
import debounce from 'underscore/modules/debounce';




function CourtView() {
  var etape = 0;
  function changeScrollNb(px) {
    document.getElementById("rowsNb").scrollTo(0, px);
  }

  function height(text) {
    var lignes = text.split('\n');
    var reponse = lignes.map((l, i) => (i + 1).toString().padStart(2, '0') +
      "\n").join('');
    return reponse;
  }
  //  var throttleUpdate = throttle(Courts.update,1000); 
  //  var debounceUpdate = debounce(Courts.update,1000); 
  return {
    onbeforeremove: function(vnode) {
      console.log("onbeforeremove of Courtview")
        Courts.putCourts(vnode.attrs.id);
    },
    view: function(vnode) {
      //console.log(vnode.attrs)
      if (Object.values(Courts.liste).length == 0 || Courts.liste[vnode.attrs
          .id] === undefined) {
        setTimeout(m.redraw, 300);
        /*if (Courts.liste[id] === undefined) {
          console.log("is undefine ?")
        }*/
        return m(".enAttente", "En attente de chargement...")
      }
      else {
        var id = vnode.attrs.id;
        var court = Courts.liste[id];
        vnode.attrs.type = court.type
        vnode.attrs.court = court;
        function ProcessusEditor(){
          
          
          //vnode.attrs.type = court.type
          //vnode.attrs.court = court;
          var pack14 = court.content.split('\n').reduce((acc,l,i)=>{
            if(i%14==0){
              acc.push([l]);
            }
            else{
              acc[acc.length-1].push(l);
            }
            return acc;
          },[]).map(s=>s.join('\n'));
          console.log(pack14)
          return m("div.block",{style:"display:flex;height:100%"},
          m("div",{style:"padding:10px",onclick:()=>{etape--}},"<"),
          m(".poeme",{style:"width:48%"},
            //court.title!=""?m(".titre",court.title):null,
            m(".strophe", ((etape>0)?pack14[etape-1]:"").split(/\r?\n/)
              .map((v, i) => m('p.vers', v)))),
          m("textarea.textarea.is-small.editvers", {
            style:"width:48%;min-width:0px",      
            value: pack14[etape],
            //                        onblur:(e)=>{                            
            //                          Courts.update(id,nson,'content-'+i,e.target.value);
            //                          },
            onblur: () => Courts.putCourts(id),
            onkeyup: (e) => {
              e.preventDefault();
              var txt = pack14.slice();
              txt[etape]=e.target.value
    
              //console.log(e.code)
              Courts.update(id, 'content', txt.join('\n'))
            }
          }),
          m("div",{style:"padding:10px",onclick:()=>{etape++}},">")
          )
        }
      
    
    
        //console.log(id,nson)
        /*console.log(sonnet.content
                          .split(/\r?\n/)
                          .slice(14)
                                                    .reduce((acc,l)=>
                            (l.length==0)?[...acc,l]:[...acc.slice(0,acc.length-1),acc[acc.length-1]+'\n'+l],[""])

                      )*/
        //console.log(premVersStrophe)
        var contenu = m("#editor",
            
            m("#editcontent.block.twothird",
              m("#choixForme.select.is-small",
              m("select.is-small", {
                name: "forme",
                onchange: (e) => Courts.update(id, 'type', e.target.value)
              }, ['trident', 'haiku','sonnet', 'processus', 'autre'].map((f) => m('option' + ((
                court.type == f) ? '[selected]' : ''), {
                value: f
              }, f)))
            ),
                (court.type=='processus')?ProcessusEditor():m("textarea.textarea.is-small.editvers", {
                 
                  value: court.content,
                  //                        onblur:(e)=>{                            
                  //                          Courts.update(id,nson,'content-'+i,e.target.value);
                  //                          },
                  onblur: () => Courts.putCourts(id),
                  onkeyup: (e) => {
                    e.preventDefault();
                    var txt = e.target.value;
                    //console.log(e.code)
                    Courts.update(id, 'content', e.target.value)
                  }
                })
              
            ),
            m(".comment.onethird",
              
            m("#titleCom","Commentaires:"),
              
                m("textarea.textarea", {
                  
                  value: court.comment,
                  onblur: () => Courts.putCourts(id),
                  oninput: (e) => {
                    e.preventDefault();
                    Courts.update(id, 'comment', e.target.value)
                  }
                })
              
            )
          )
          //console.log(snark)
        return m(".maxheight.container.is-max-desktop.box",{onblur:()=>Courts.putCourts(id)},
          m("input[type=text]#edittitle.input.has-text-weight-bold", {
                value: court.title,
                onblur: () => Courts.putCourts(id),
                onkeyup: (e) => {
                  e.preventDefault();
                  var txt = e.target.value;
                  //console.log(e.code)
                  if (e.code == "Enter") {
                    Courts.update(id, 'title', txt);
                    focus(0);
                  }
                  else {
                    Courts.update(id, 'title', e.target.value)
                  }
                }
              }),
            contenu
          )
        }
      }
    }
  }
  
  
  module.exports = CourtView

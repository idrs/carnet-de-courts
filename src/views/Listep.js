import m from "mithril";
import Courts from "../models/Courts"
import {
  marked
}
from "marked";
import dompurify from "dompurify";

function md2html(txt) {
  return dompurify.sanitize(marked.parse(txt));
}

function comment(court) {
    var active = false;
    return {
      
      view: function() {
        
        return m(".comment.italic",
          m('span.clique', {
            onclick: () => {
              console.log("clicked: active="+active);
             if(active)
             {active = false;}
             else { active = true;}
             console.log("clicked: active="+active);
            }
          }, '+'),
          m(active ? 'span.active' : 'span.hidden', m.trust(md2html(court.comment)))
        )
      }
    }
  }

  function publication(court){
  
  return   m("span.publication.tag",{
    onclick:()=>{Courts.togglePublic(court.id)}
  },
                court.public?"partagé":"privé"
            );
  }
  
  function contenu(court) {
    //console.log(court)
    var trid = court.type == "trident" 
    return [
      //m(".forme",sonnet.forme),
      m(".poeme.block",
        //court.title!=""?m(".titre",court.title):null,
        m(".strophe", court.content.split(/\r?\n/)
          .map((v, i) => m('p.vers' + ((trid && (i != 1)) ? '.plus' : ''), ((trid && (i == 1)) ?
            '⊗ ' : '') + v))))
    //,  m(comment(court))
    ]
  }

function Listep(vnode) {
  //Courts.loadPublicDb(vnode.attrs.adress)
  console.log("attrs:",vnode.attrs)
  var liste = {};
  function versDate(n) {
    var options = {
      year: 'numeric',
      month: 'long',
      day: 'numeric'
    };
    const d = new Date(n);
    return d.toLocaleDateString("fr-FR", options);
  }

  m.request(
    {
    method: "GET",
    url: "https://"+vnode.attrs.adress+"/_all_docs",
    params: {},
    body: {}
})
.then(function(result) {
    console.log(result.rows);
    //var listsOfDocs = JSON.parse(result)
    result.rows.forEach((e)=>
        {console.log(e);
        m.request({
          method: "GET",
          url: "https://"+vnode.attrs.adress+"/"+e.id
        })
        .then(
          (res)=>{
            //var resObj = JSON.parse(res);
            if(!res._id.startsWith('_design'))
            liste[res._id]=res;
            m.redraw();
          }
        )
      })
}).catch((err)=>{console.log("request err:",err);});

  
  
      var visible = 20;
  return {
    view: function() {
      //   console.log("liste: ",Object.values(Courts.liste));
            console.log(Courts)

      return m(".container.is-max-desktop",{onscroll:(e)=>{console.log("scrolling !");}},
                m("ul",
        Object.values(liste).sort((a,b)=>{parseInt(a._id,16)-parseInt(b._id,16)})
        .map((s,i)=>[s,i]).reverse().slice(0,visible).map((arr) =>
            m('li.box',
              m("h4.bold",
                 (arr[1] + 1) + ". " +arr[0].title
              ),
              contenu(arr[0]),
              m(".italic.small", versDate(parseInt(arr[0]._id, 16))),
             // m("span.bold", arr[0].type || "?"),
              //m("div",s.content)
            )),
            (Object.values(liste).length>visible)?
              m("li.box",{onclick:()=>{visible=visible+10}},m(".bold","Suivants")):
                null
              ))
    }
  }
};
module.exports = Listep;

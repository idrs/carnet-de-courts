import m from "mithril";
import Courts from "../models/Courts"



function Config() {
  
  var config = Courts.config || {url:'',nom:'',mdp:'',urlpublic:''};
  
  return {
    view: function(vnode) {
      
      
        return m(".container.is-max-desktop.box",
          [
           m(".lbl",'Base de données:'),
           m("input[type=text].input", {
                value: config.url,
                oninput: (e) => {
                  e.preventDefault();
                  config.url = e.target.value;
                }
              }),
           m(".lbl",'Identifiant:'),
           m("input[type=text].input", {
                value: config.nom,
                oninput: (e) => {
                  e.preventDefault();
                  config.nom = e.target.value;
                }
              }),
            m(".lbl",'Mot de passe:'),
           m("input[type=text].input", {
                value: config.mdp,
                oninput: (e) => {
                  e.preventDefault();
                  config.mdp = e.target.value;
                }
              }),
            m(".lbl",'Base de données publique:'),
           m("input[type=text].input", {
                value: config.urlpublic||"",
                oninput: (e) => {
                  e.preventDefault();
                  config.urlpublic = e.target.value;
                }
              }),
            m("button.button",{onclick:()=>Courts.saveConfig(config)},"Enregistrer") 
          ]
          )
        }
      }
    }
  
  
  
  module.exports = Config

import m from 'mithril';

import Accueil from "./views/Accueil";
import Layout from "./views/Layout";
import Liste from "./views/Liste";
import Listep from "./views/Listep";
import Config from "./views/Config";
import Courts from "./models/Courts";
import CourtView from "./views/CourtView";
import Debug from "./views/Debug";
import DbView from "./views/DbView";

//console.log('Hello world!');



m.route(document.getElementById('app'), "/", {
   "/":  { render: function(vnode){
     return m(Layout, m(Liste))}
    },
   "/court/:id":  { render: function(vnode){
     return m(Layout, m(CourtView,vnode.attrs))}
    },
    "/config":  { render: function(vnode){
     return m(Layout, m(Config))}
    },
    "/pub/:adress...": {render: function(vnode)
        {
            return m(Layout,m(Listep,vnode.attrs))
        }
    },
    "/debug": {render : function(vnode)
        {
          return m(Layout,m(Debug));
        }
    },
    "/dbview": {render : function(vnode)
        {
          return m(Layout,m(DbView));
        }
    }
});


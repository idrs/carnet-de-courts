import m from 'mithril';
//import Stream from 'mithril/stream';
//import merge from 'mergerino' 
import PouchDB from 'pouchdb'
import throttle from 'underscore/modules/throttle';


// model part

var db = new PouchDB('courts');

var publicdb = new PouchDB('public');

db.replicate.to(publicdb, {
    live: true,
    retry: true,
    filter: function(doc) {
        console.log(doc._id,doc.public)
        return doc.public;
    }
}).then(function(result) {
  console.log("completed replication:",result)
    // handle 'completed' result
}).catch(function(err) {
    console.log(err);
});



const Courts = {

    isLoading: false,
    loading: function(bool) {
        Courts.isLoading = bool;
        //console.log("loading...",bool);
        //m.redraw();
    },
    config: {url:'',nom:'',mdp:'',urlpublic:''},
    loadConfig: function() {
        Courts.config = JSON.parse(localStorage.getItem('courts-config'));
        console.log(Courts.config)
        if(Courts.config && !Courts.config.urlpublic){Courts.config["urlpublic"]="";}
    },
    saveConfig: function(config) {
        localStorage.setItem('courts-config', JSON.stringify(config));
        loadRemoteDB();
    },
    //onview: 0,
    loadCourts: function(redraw) {
        Courts.loading(true);
        //console.log("loadCourts");
        db.allDocs({
                include_docs: true
            })
            .then((result) => {
                //console.log("load results: ",result);
                result.rows.forEach((r) => {
                    Courts.liste[r.id] = r.doc

                });
                Courts.loading(false);
                if (redraw) {
                    m.redraw()
                }
            })
            .catch((err) => console.log(err));
        publicdb.allDocs({
                include_docs: true
            })
            .then((result) => {
                //console.log("load results: ",result);
                result.rows.forEach((r) => {
                    if(!r.id.startsWith('_design')){Courts.listepub[r.id] = r.doc}
                });
                Courts.loading(false);
                if (redraw) {
                    m.redraw()
                }
            })
            .catch((err) => console.log(err));
    },
    putCourts: throttle(function(id) {
        //console.log("putCourts");

        //console.log("bulk launched")
        db.put(Courts.liste[id])
            .then((result) => {
                //console.log(result.ok);
                Courts.loadCourts()
            })
            .catch((err) => console.log(err));
    }, 1000),
    liste: {},
    listepub: {},
    loadPublicDb: function() {
        //ajouter obfuscation adresse
        var remotePublicDB = new PouchDB('https://' + Courts.config.nom + ':' + Courts.config.mdp + '@' + Courts.config.urlpublic);

        publicdb.sync(remotePublicDB, {
            live: true,
            retry: true
        }).on('change', function(change) {
            // yo, something changed!
            console.log("publicdb change:",change);
            Courts.loadCourts(true);
        }).on('paused', function(info) {
            // replication was paused, usually because of a lost connection
            Courts.log("paused sync", info)
        }).on('active', function(info) {
            // replication was resumed
            Courts.log("resumed sync", info)
        }).on('error', function(err) {
            // totally unhandled error (shouldn't happen)
            Courts.log("error sync, problem!", err)
        });
    },
    newCourt: function() {
        // creer un nouveau poeme
        var s = {}
        var d = new Date();
        s._id = d.getTime().toString(16);
        s.title = '';
        s.type = 'autre';
        s.content = '';
        s.comment = '';
        s.public = false;
        //console.log('type',type)
        console.log(Courts)

        //console.log(s)
        db.put(s).then((rep) => {
            //console.log(rep);
            Courts.loadCourts(true)
        }).catch((err) => console.log(err));

        //this.liste[s._id]=s;
        return s._id
    },
    togglePublic: function(id) {
        console.log("toggle", id)
        if (Courts.liste[id].public) {
            Courts.liste[id].public = false;
        } else {
            Courts.liste[id].public = true;
        }
        Courts.putCourts(id);
        if (Courts.liste[id].public) {
            console.log("poeme publié")
        } else {
            console.log("tentative d'effacement")
            publicdb.get(id).then((doc) => {
                console.log('remove public', doc)
                publicdb.remove(doc).then(() => {
                    Courts.loadCourts(true)
                });
            }).catch(err => Courts.log("erreur après remove public", err))
        }

    },
    update: function(id, partie, texte) {
        Courts.liste[id][partie] = texte;
    },
    delete: function(id) {
        //delete Courts.liste[id];
        db.get(id).then((doc) => {
            db.remove(doc);
        }).catch(err => Courts.log("erreur après remove", err))
    },
    logfile: [],
    log: function(...logText) {
        var d = new Date();
        Courts.logfile.push(d + ':\n' + logText.join(' '))
    },
    dbList:[],
    publicDbList:[],
      rdbList : [],
      rpublicDbList : [],
    getDb: function(){
      console.log("calling getDB");
      dbList = [];
      publicDbList = [];
      db.allDocs({
                include_docs: true
            })
            .then((result) => {
                //console.log("load results: ",result);
                result.rows.forEach((r) => {
                    Courts.dbList.push(r.doc);
                    m.redraw()
                });
            })
            .catch((err) => console.log(err));
      publicdb.allDocs({
                include_docs: true
            })
            .then((result) => {
                //console.log("load results: ",result);
                result.rows.forEach((r) => {
                    Courts.publicDbList.push(r.doc);
                    m.redraw()
                });
            })
            .catch((err) => console.log(err));
      
    }
}

Courts.loadConfig();

function loadRemoteDB() {
    if (Courts.config) {
        var remoteDB = new PouchDB('https://' + Courts.config.nom + ':' + Courts.config.mdp + '@' + Courts.config.url);

        db.sync(remoteDB, {
            live: true,
            retry: true
        }).on('change', function(change) {
            // yo, something changed!
            Courts.loadCourts(true);
            Pouchdb.replicate(db,publicdb, {
    live: true,
    retry: true,
    filter: function(doc) {
        console.log(doc._id,doc.public)
        return doc.public;
    }
}).then(function(result) {
  console.log("completed replication:",result)
    // handle 'completed' result
}).catch(function(err) {
    console.log(err);
});
        }).on('paused', function(info) {
            // replication was paused, usually because of a lost connection
            Courts.log("paused sync", info)
        }).on('active', function(info) {
            // replication was resumed
            Courts.log("resumed sync", info)
        }).on('error', function(err) {
            // totally unhandled error (shouldn't happen)
            Courts.log("error sync, problem!", err)
        });
        if (Courts.config.urlpublic.length > 0) {
            Courts.loadPublicDb();
        }
    }
}

loadRemoteDB();

Courts.loadCourts();

//console.log("listepub:",Courts.listepub);

m.redraw();

module.exports = Courts
